const listBlock = document.querySelector('.list');
let users = [];


//Чтобы работать с запросами мы будем обращаться к серверу по API - по конкретному url будет конкретно сервер будет понимать, что мы хотим от него. 
// если бы не было на стороне сервера настроек мы бы запрашивали все, но уже в url можно указывать например надо только мужчины или только 
// женщины или только первые 10. Обычно в конце url /api 
// Способы указать параметры:
// 1 способ - прямо в api - query-параметры
// 2 способ в теле запросе. 
// Пример /api ?results=10 - первые 10 объектов. 
// Пример /api ?results=10&gender=male - первые 10 объектов мужского пола. 
// Методы: GET (получить)

const xhr = new XMLHttpRequest() //инструмент через который мы делаем запрос. fetch - современная обёртка. Чтобы его создать надо сделать экземпляр этого класса
listBlock.classList.add('loading'); 
listBlock.addEventListener('click', onListClick);//когда кликаем - в любом месте вызывается данная функция, 


// для этого используем слово new. xhr - константа. 
// xhr.OPENED - открывает запрос
// xhr.open('GET','https://randomuser.me/api'); //get - первый метод(получить). чтобы метод завершился надо написать xhr send
// xhr.send();
// узнать что запрос ушел - во вкладке network. Один из сигналов что запрос прошел успешно status 200. 400 - что-то не найдено. 500 - ошибка бэка

//c 1 неинтересно, допишем ?results=10
// xhr.open('GET','https://randomuser.me/api?results=50&gender=male');
// xhr.send();

xhr.open('GET', 'https://randomuser.me/api?results=100');
xhr.send(); // место расположения не важно. Пока мы не выполним xhr.onreadystatechange = function - ничего не произойдет

// чтобы сохранить куда-то от запроса - надо обработать это событие. Проходит какое-то время. Из-за этого времени нельзя просто 
// написать const data = xhr.send(). Из-за этого времени задержки ответа сервиса нельзя работать напрямую. Promise - фактически особый 
// тип данных, который обещает, что данные скоро будут. Чтобы узнать: onreadystatechange - функция которая работает каждый раз при смене статуса
// запроса

//xhr.onreadystatechange = function () {}; 
//xhr.addEventListener('readystaychange', func);  мы вешаем слушатель на изменение статуса запроса. когда оно происходить мы вызываем 
// коллбэк функция. Эти две записи работают одинаково. На каждое изменение функции срабатывает функция. Первый вариант чаще исп. в доках.  

//проверка работоспособности
// xhr.onreadystatechange = function () {
//     console.log('state change'); // вывелась 3 раза
// }; 
// Где брать информацию, что данные прилетели? Где взять эту цифру от 0-4? она лежит в xhr.readyState - состояние готовности. 4 - показатель что все готово


// xhr.onreadystatechange = function () {
//     console.log('state change', xhr.readyState); // вывелась 4
// }; 

xhr.onreadystatechange = function () {
    console.log('state change', xhr.status); // xhr.status - проверка статуса
    if (xhr.readyState === 4) { // проверяем по 4 - чтобы запрос прошел, а когда он закончен проверяем его статус
        if (xhr.status === 200) { // статус - 200 показатель, что все хорошо
            const response = xhr.responseText; //сюда ложем ответ сервера. Откуда из брать? xhr.responseText
            //console.log(response); //проверка данных. возвращает JSON строку. надо парсить
            const data = JSON.parse(response); //парсим данные. 
            users = data.results; 
            // console.log(users);
            console.log(data); //проверка того что распарсилось.
            rednderUsers(data.results); //добавляем к data results - потому что там лежит 2 объекта results и info
            listBlock.classList.remove('loading'); //когда загрузка закончилась - убираем класс.
        }
    }
};

//info нам не надо. поэтому в функции отрисовки мы будем отрезать info объектов.

//функции:
//1. генерирует карточки
//2. функция перебирает элементы массива, на каждой итерации будет обращаться к элементу массива и генерировать карточки. 

//если есть get - должен быть return. в user - будет лежать 1 объект. Функция будет получать из result - 1 объект. Она не знает, что будет 
// участвовать в итерации. И на выходе она будет выплёвывать (размещать) разметку
function getUserCard(user) {
    const card = getTag('div', 'user');
    const info = getTag('div', 'info');
    const fullName = user.name.first + ' ' + user.name.last; // мы забираем данные из объекта, которые потом будем использовать
    const name = getTag('span', 'name', fullName);
    const email = getTag('span', 'email', user.email);
    const age = getTag('span', 'age', user.dob.age);
    const avatar = getTag('img', 'avatar');
    avatar.src = user.picture.medium;
    const deleteBtn = getTag('div','delete','X');//кнопка удаления
    card.id = user.login.uuid; //берем тэг div с классов card и добавляем к нему id.
    //собираем матрёшку
    info.append(name, email, age);
    card.append(avatar, info, deleteBtn);

    return card;
}

// возвращает тэг, класс и контент. content=null - установка параметра по умолчанию. Все необязательное в конец 
function getTag(tagName, className, content = null) {
    const newTag = document.createElement(tagName);
    newTag.classList.add(className);
    newTag.innerText = content;
    return newTag;
};

//функция которая пробегает по массиву 

function rednderUsers(users) {
    listBlock.innerHTML = '';
    users.forEach(item => {
        const userCard = getUserCard(item);  //сюда записываем результат выполнения 1 выполнения getUserCard. Получает данные и запихивает в html код
        listBlock.append(userCard);
    });

    // 2 метод с копированием нового массива и его разделением
    // const userCards = user.map(item => getUserCard(item));
    // listBlock.append(...userCards);
    //Метод map - на основании существующего массива формирует новый массив. На каждой итерации мы можем сделать что-то. например item+2 - все бы элементы были бы увеличены на двойку.
    // на каждой итерации массива происходит вызов функции в которую передается объект. А далее ... - массив разбивается
};

// все события всплывают. у нас есть обертка на которую можно повесить клик и до нее все равно дойдет. Но в этом событии будет лежать в свойстве таргет на что мы нажали. 
// Мы можем узнать от туда что-нибудь. 


//функция колбэк на клик. 

function onListClick(event) {
   //console.log(event);  отслеживаем событие. Откуда достать нам данные. Далее добавляем слушать на наш listbloc. В консоле будет отображаться событие.
    // разворачиваем его и ищем в target - элемент на который нажали. div.delete - если нажать на крестик
    if (event.target.className === 'delete'){ // если класс delete - то выводит консоль
        // console.log('Delete');  выводит Delete если кликаем по элементу с классом delete
        // console.log(event.target.parentNode.id); 
        deleteUser(id);
    }else {
        // alert('Мимо!'); 
    }
}

//здесь будет прилетать user Id, полученный из клика
function deleteUser(userID) {
    users = users.filter(item => item.login.uuid != userId); //все юзеры у которых id не совпадают остаются, а тот у которого совпадает исключается
    rednderUsers(users);//отрисовали с новым массивом уже без того по которому кликнули
}